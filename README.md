# Readme

Einfaches Skript um die Geschwindigkeit der verschiedenen Speichervarianten einer dünnbesetzten Matrix in Numpy zu testen.

Die Matrix wird durch die Addition einer Bandmatrix mit einer Hauptdiagonalen und zwei Nebendiagonalen sowie einer zufälligen dünnbesetzen Matrix konstruiert.

Die Zeit zur Konstruktion der Matrix wird nicht beachtet.

## Installation

```
$ pip install pipenv
$ git clone https://gitlab.com/Feorg/sparsespeedtest
$ cd sparsespeedtest
$ pipenv install
$ pipenv shell
$ python sparsetest.py
```

## Options

Folgende Paramter stehen zur Auswahl:
* -s size  
  Default: 15  
  Dimension der Matrix.
* -d density  
  Default: 0.1  
  Gibt an wie dicht die dünnbesetzte Matrix besetzt sein soll. Muss in [0,1] liegen.
* -f mformat  
  Default: ['csc', 'csr', 'bsr', 'lil', 'dok', 'coo', 'dia', 'dense']  
  Auswahl der Speichermöglichkeiten.
* -r repeats  
  Default: 5  
  Wie oft die Multiplikation ausgeführt wird.
* -g graph  
  Default: 0  
  Zur Anzeige des Plots `1` setzen.
* -p print  
  Default: 0  
  Zur Anzeige des Plots `1` setzen. Bei großen Matrizen nicht praktikabel.

## Beispiel

Um eine 10x10-Matrix zu erzeugen, im Terminal anzuzeigen, die Speichermethoden _csr_ und _dense_ zu vergleichen und es anschließend graphisch ausgeben zu lassen würde der Befehl lauten:
```
$ python sparsetest.py -s 10 -p 1 -g 1 -f csr dense
```

## Bemerkungen

* Es wird eine gleiche Stufe der Optimierung bei allen Verfahren angenommen.
* Es wird nur ein einfacher Zeitvergleich ausgeführt. Für genauere Ergebnisse müsste nachgebessert (profiling, etc) werden.
* Konstruktion der Matrix wird nicht berücksichtigt => Zeitaufwand zur Konstruktion der Matrix und eventuelles umwandeln müssen in der Praxis berücksichtigt werden.
* Um das Notebook verwenden zu können muss `$ pipenv install --dev` ausgeführt werden um Jupyterlab zu installieren.
* 'festes' zufälliges Format, welches sich nicht an der Praxis orientieren muss -> evtl händische Optimierungen möglich.