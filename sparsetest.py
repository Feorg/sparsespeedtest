import numpy as np
import scipy as sp
import scipy.sparse as sparse
import profile
import time
import matplotlib.pylab as plt
import argparse
import sys
np.set_printoptions(linewidth=200)
np.set_printoptions(threshold=sys.maxsize)

def test_sparse(A):
    if sparse.isspmatrix(A):
        if sparse.isspmatrix_csc(A):
            return 'csc'
        elif sparse.isspmatrix_csr(A):
            return 'csr'
        elif sparse.isspmatrix_bsr(A):
            return 'bsr'
        elif sparse.isspmatrix_lil(A):
            return 'lil'
        elif sparse.isspmatrix_dok(A):
            return 'dok'
        elif sparse.isspmatrix_coo(A):
            return 'coo'
        elif sparse.isspmatrix_dia(A):
            return 'dia'
        else:
            return 'sparse'
    else:
        if isinstance(A, np.ndarray):
            return 'dense'
        elif isinstance(A, list):
            return 'list'
        else:
            return f'{type(A)}'

def make_sparse(A, mformat='csr'):
    if mformat=='csc':
        return sparse.csc_matrix(A)
    elif mformat=='csr':
        return sparse.csr_matrix(A)
    elif mformat=='bsr':
        return sparse.bsr_matrix(A)
    elif mformat=='lil':
        return sparse.lil_matrix(A)
    elif mformat=='dok':
        return sparse.dok_matrix(A)
    elif mformat=='coo':
        return sparse.coo_matrix(A)
    elif mformat=='dia':
        return sparse.dia_matrix(A)
    elif mformat=='dense':
        return A.todense()
    else:
        print("no known sparse-format")
        return A

def make_sparse_list(A, mformat):
    if isinstance(mformat, str):
        mformat = [mformat]
    sparse_list = []
    for form in mformat:
        sparse_list.append(make_sparse(A,form))
    return sparse_list

def test_dot_time(A,b,mformat,repeats=1):
    sparse_list = make_sparse_list(A, mformat)
    time_list = []
    time_dic = {}
    for mat in sparse_list:
        time_start = time.process_time()
        for i in range(repeats):
            mat.dot(b)
        time_end = time.process_time()
        time_elapsed = (time_end - time_start)/repeats
        time_dic[test_sparse(mat)]=time_elapsed
    return time_dic

def create_data(size_matrix = 5, density = 0.1):
    diagonals = [np.random.rand(size_matrix), np.random.rand(size_matrix-1), np.random.rand(size_matrix-1)]
    A_band = sparse.diags(diagonals, [0, -1, 1])
    A_sparse = sparse.rand(m=size_matrix, n=size_matrix, density=density)
    A = A_sparse + A_band
    b = np.random.rand(size_matrix)
    return (A,b)

def plot_time_dic(time_dic):
    plt.bar(range(len(time_dic)), time_dic.values(), align='center')
    plt.xticks(range(len(time_dic)), list(time_dic.keys()))
    plt.show()

def print_time_dic(time_dic):
    print('******* time elapsed for multiplication *******')
    for key, val in time_dic.items():
        print(f'{key:<5}: {val:1.3e}')

mformat_list_org = ['csc', 'csr', 'bsr', 'lil', 'dok', 'coo', 'dia', 'dense']

parser = argparse.ArgumentParser(description='Zeitvergleich Matrix-Vektor Multiplikation von verschiedenen Sparse Matrizen.\nMatrix wird erzeugt durch addition einer Bandmatrix mit Hauptdiagonale und 2 Nebendiagonalen sowie einer sparse Matrix mit zu waehlender Besetzungsdichte')
parser.add_argument('-s', '--size', type=int, default=15, help='Dimension der Matrix')
parser.add_argument('-d', '--density', type=float, default=0.1, help='in [0,1], wie dicht die Matrix besetzt ist')
parser.add_argument('-f', '--mformat', nargs='+', default=mformat_list_org, choices=mformat_list_org, help='Liste mit verschiedenen sparse Modellen')
parser.add_argument('-r', '--repeats', type=int, default=5, help="Anzahl der Matrix-Vektor-Multiplikatoren bevor der Mittelwert ueber die Zeit gebildet wird")
parser.add_argument('-g', '--graph', type=int, default=0, help='graphische Ausgabe. Default: 0 (false)')
parser.add_argument('-p', '--print', type=int, default=0, help='print sparse matrix. Default: 0 (false)')

args = parser.parse_args()

size_matrix = args.size
density = args.density
repeats = args.repeats
graph = args.graph
print_A = args.print
mformat = args.mformat

A,b = create_data(size_matrix=size_matrix, density=density)

if print_A:
    print('******* sparse matrix *******')
    print(A.todense())
    print()

time_dic = test_dot_time(A, b, mformat, repeats=repeats)
print_time_dic(time_dic)
if graph:
    plot_time_dic(time_dic)